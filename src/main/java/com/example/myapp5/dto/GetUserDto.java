package com.example.myapp5.dto;

import lombok.Data;

@Data
public class GetUserDto {
    private Integer id;
    private String username;
    private String email;
    private String password;
    private String confirmPassword;
}
