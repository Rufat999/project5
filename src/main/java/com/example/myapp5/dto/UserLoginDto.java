package com.example.myapp5.dto;

import lombok.Data;

@Data
public class UserLoginDto {
    private String email;
    private String password;
}
