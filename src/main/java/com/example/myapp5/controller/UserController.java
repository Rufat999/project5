package com.example.myapp5.controller;

import com.example.myapp5.dto.GetUserDto;
import com.example.myapp5.dto.UserLoginDto;
import com.example.myapp5.dto.UserRegisterDto;
import com.example.myapp5.dto.UserResetPasswordDto;
import com.example.myapp5.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user/{id}")
    public GetUserDto getUserById(@PathVariable Integer id){
        return userService.getUserById(id);
    }

    @GetMapping("/user/all")
    public List<GetUserDto> getAllUsers(){
        return userService.getAllUsers();
    }

    @PostMapping("/user/register")
    public void userRegister(@RequestBody UserRegisterDto userRegisterDto){
        userService.userRegister(userRegisterDto);
    }

    @PostMapping("/user/login")
    public void userLogin(@RequestBody UserLoginDto userLoginDto){
        userService.userLogin(userLoginDto);
    }

    @PutMapping("/user/resetPassword")
    public void userResetPassword(@RequestBody UserResetPasswordDto userResetPasswordDto){
        userService.userResetPassword(userResetPasswordDto);
    }

    @DeleteMapping("/user/{id}")
    public void deleteById(@PathVariable Integer id){
        userService.deleteById(id);
    }

    @DeleteMapping("/user/all")
    public void deleteAllUsers(){
        userService.deleteAllUsers();
    }
}
