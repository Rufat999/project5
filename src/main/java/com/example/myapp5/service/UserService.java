package com.example.myapp5.service;

import com.example.myapp5.dto.GetUserDto;
import com.example.myapp5.dto.UserLoginDto;
import com.example.myapp5.dto.UserRegisterDto;
import com.example.myapp5.dto.UserResetPasswordDto;

import java.util.List;

public interface UserService {
    GetUserDto getUserById(Integer id);

    List<GetUserDto> getAllUsers();

    void userRegister(UserRegisterDto userRegisterDto);

    void userLogin(UserLoginDto userLoginDto);

    void userResetPassword(UserResetPasswordDto userResetPasswordDto);

    void deleteById(Integer id);

    void deleteAllUsers();
}
