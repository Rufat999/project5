package com.example.myapp5.service;

import com.example.myapp5.dto.GetUserDto;
import com.example.myapp5.dto.UserLoginDto;
import com.example.myapp5.dto.UserRegisterDto;
import com.example.myapp5.dto.UserResetPasswordDto;
import com.example.myapp5.entity.User;
import com.example.myapp5.exception.*;
import com.example.myapp5.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public GetUserDto getUserById(Integer id) {
        User user = userRepository.findById(id).get();
        GetUserDto getUserDto = modelMapper.map(user, GetUserDto.class);
        return getUserDto;
    }

    @Override
    public List<GetUserDto> getAllUsers() {
        List<User> users = userRepository.findAll();

        List<GetUserDto> usersDtoList = users.stream().map((user) -> modelMapper.map(user, GetUserDto.class)).collect(Collectors.toList());
        return usersDtoList;
    }

    @Override
    public void userRegister(UserRegisterDto userRegisterDto) {
        Optional<User> email = userRepository.findByEmail(userRegisterDto.getEmail());

        if (email.isPresent()) {
            throw new UserAlreadyExistsException("User already exists!");
        }

        if (userRegisterDto.getUsername().equals("") || userRegisterDto.getEmail().equals("") ||
                userRegisterDto.getPassword().equals("") || userRegisterDto.getConfirmPassword().equals("")) {
            throw new NotNullException("Enter something!");
        }

        if (!Objects.equals(userRegisterDto.getPassword(), userRegisterDto.getConfirmPassword())) {
            throw new PasswordMatchException("Password Match Exception");
        }

        User user = modelMapper.map(userRegisterDto, User.class);
        userRepository.save(user);
    }

    @Override
    public void userLogin(UserLoginDto userLoginDto) {

        Optional<User> checkUserExistsInDatabase = userRepository.findByEmailAndPassword(userLoginDto.getEmail(), userLoginDto.getPassword());
        if (checkUserExistsInDatabase.isPresent()) {
            System.out.println("Successfully Complete");
        } else {
            throw new UserNotExistsException("Email or Password Not True");
        }
    }

    @Override
    public void userResetPassword(UserResetPasswordDto userResetPasswordDto) throws NotNullException {
        Optional <User> userExistsInDatabase = userRepository.findByEmail(userResetPasswordDto.getEmail());

        if (userExistsInDatabase.isPresent()) {
            if (Objects.equals(userResetPasswordDto.getPassword(), userResetPasswordDto.getConfirmPassword())) {
                userExistsInDatabase.map(user -> {
                    user.setUsername(user.getUsername());
                    user.setEmail(user.getEmail());
                    user.setPassword(userResetPasswordDto.getPassword());
                    user.setConfirmPassword(userResetPasswordDto.getConfirmPassword());
                    return userRepository.save(user);
                });
            }
            else {
                throw new PasswordMatchException("Password Match Exception!");
            }
        }
        else if (userExistsInDatabase.isEmpty()){
            throw new UserNotExistsException("User Not Exists!");
        }
    }

    @Override
    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteAllUsers() {
        userRepository.deleteAll();
    }
}