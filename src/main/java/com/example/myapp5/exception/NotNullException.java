package com.example.myapp5.exception;

public class NotNullException extends RuntimeException{
    private String message;

    public NotNullException(String message){
        super(message);
    }
}
