package com.example.myapp5.exception;

public class PasswordMatchException extends RuntimeException{
    private String message;

    public PasswordMatchException(String message){
        super(message);
    }
}
