package com.example.myapp5.exception;

public class UserNotExistsException extends RuntimeException{
    private String message;

    public UserNotExistsException(String message){
        super(message);
    }
}
