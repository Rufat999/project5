package com.example.myapp5.exception;

public class PasswordAndConfirmPasswordNotEqualException extends RuntimeException{
    private String message;

    public PasswordAndConfirmPasswordNotEqualException(String message){
        super(message);
    }
}
